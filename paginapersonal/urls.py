from django.conf.urls import include, url
from . import views

urlpatterns = [
    #url(r'^$', views.post_list),
    url(r'^$', views.home, name="home"),
    url(r'^contacto/$', views.contacto, name='contacto'),
    url(r'^portafolio/$', views.portafolio, name='portafolio'),
    url(r'^acercademi/$', views.acercademi, name='acercademi'),
    ]
