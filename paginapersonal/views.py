from django.shortcuts import render
from django.utils import timezone
from .models import Post
# Create your views here.
#def post_list(request):
    #    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    #    return render(request, 'paginapersonal/post_list.html', {'posts': posts})

def home(request):
    return render(request, "paginapersonal/home.html")
def contacto(request):
    return render(request, "paginapersonal/contacto.html")
def portafolio(request):
    return render(request, "paginapersonal/portafolio.html")
def acercademi(request):
    return render(request, "paginapersonal/acercademi.html")
